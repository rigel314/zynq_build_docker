FROM ubuntu:20.04

RUN apt-get update -y -qq && apt-get install -y -qq --no-install-recommends \
	curl wget p7zip-full gcc-arm-none-eabi gcc-arm-linux-gnueabihf build-essential libssl-dev ca-certificates gnupg bison flex vim-common bc git cpio \
	&& rm -rf /var/lib/apt/lists/*

COPY bootgen /usr/bootgen

RUN cd /usr/bootgen/; make -j$(nproc); cp bootgen /usr/local/bin; make clean

ENV PATH /usr/local/go/bin:$PATH

RUN set -eux; \
	arch='linux-amd64'; \
	url='https://storage.googleapis.com/golang/go1.15.4.linux-amd64.tar.gz'; \
	sha256='eb61005f0b932c93b424a3a4eaa67d72196c79129d9a3ea8578047683e2c80d5'; \
	wget -O go.tgz.asc "$url.asc" --progress=dot:giga; \
	wget -O go.tgz "$url" --progress=dot:giga; \
	echo "$sha256 *go.tgz" | sha256sum --strict --check -; \
	\
	# https://github.com/golang/go/issues/14739#issuecomment-324767697
	export GNUPGHOME="$(mktemp -d)"; \
	# https://www.google.com/linuxrepositories/
	gpg --batch --keyserver ha.pool.sks-keyservers.net --recv-keys 'EB4C 1BFD 4F04 2F6D DDCC EC91 7721 F63B D38B 4796'; \
	gpg --batch --verify go.tgz.asc go.tgz; \
	gpgconf --kill all; \
	rm -rf "$GNUPGHOME" go.tgz.asc; \
	\
	tar -C /usr/local -xzf go.tgz; \
	rm go.tgz; \
	go version

ENV GOPATH /go
ENV PATH $GOPATH/bin:$PATH
RUN mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH"
